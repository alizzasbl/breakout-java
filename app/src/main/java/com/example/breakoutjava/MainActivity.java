package com.example.breakoutjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    BreakoutView breakout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        breakout = new BreakoutView(this);
        setContentView(breakout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        breakout.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        breakout.pause();
    }
}